from flask import Flask, render_template, url_for, request, redirect, flash
from forms import RegistrationForm
from irc_register import ircregister
#from irc_verify import ircverify


app = Flask(__name__)
app.config['SECRET_KEY'] = '$secret' #remove later

@app.route('/')
def hello():
    return render_template('home.html')

#@app.route('/kiwi')
#def kiwi():
#    return redirect("https://liberta.casa/kiwi/")
@app.route('/kiwi')
def kiwinick():
    nick = request.args.get('nick', None)
    show_password_box = request.args.get('show_password_box', None)
    redirect_url = 'https://liberta.casa/kiwi/'+'?nick='+nick+'&show_password_box'+'='+show_password_box
    return redirect(redirect_url)

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if request.method == 'POST':

        username = request.form.get('username')
        email = request.form.get('email')
        password = request.form.get('password')
#        email = request.form.get('email') add password arg to ircregisterfunction
        response = ircregister(username, password, email)
        if response == "433":
            flash("IRC username already taken. Please select a different username")
        elif response == "409":
            flash("User already exists.")
        elif response == "success":
            return redirect(url_for('kiwinick', nick=username, show_password_box='true'))
        elif response == "invalidemail":
            flash("Is that a valid email address?")
        elif response == "ssoerr":
            flash("Sorry, we messed up. Please contact an administrator.")

    return render_template('register.html', title='Register', form=form)

#@app.route('/verify', methods=['GET', 'POST'])
#def verify():
#    form = VerificationForm()
#    if request.method == 'POST':
#
#        username = request.form.get('username')
#        verif_code = request.form.get('verif_code')
#        response = ircverify(username, verif_code)
#        if response == "server failure":
#            flash("Server Unavailable")
#        elif response == "433":
#            flash("Username under use. Please check your username or visit us for help")
#        elif response == "success":
#            return redirect(url_for('kiwi'))
#        elif response == "failure":
#            flash("Failure! Please try after some time or use NickServ.")
#    return render_template('verify.html', title='Verify', form=form)


if __name__ == '__main__':
    app.run(debug=True)
